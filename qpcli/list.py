import click
from qpcli.config import config, CONFIG_FILE
from qportalwrapper.qportalwrapper import QPortalWrapper
from qportalwrapper.datatypes import Assignment, Class
from qpcli import util
from qpcli.util import print_no_server_selected_error

# Common functions
def print_current_info():
    server = config[config['DEFAULT']['server']]
    click.echo(click.style(f"Current server: {config['DEFAULT']['server']} ({server['server']})", fg="bright_cyan"))
    click.echo(click.style(f"Current user: {config['DEFAULT']['user']}", fg="bright_cyan"))
    click.echo()

def print_class(q_class: Class):
    click.echo(click.style(f"Per {q_class.period} - {q_class.name}", fg="cyan"))
    click.echo(click.style(f"  Teacher: {q_class.teacher}", fg="cyan"))
    click.echo(click.style(f"  Current Grade: {q_class.grade}", fg="cyan"))
    click.echo(click.style(f"  Grading Period: {q_class.grading_period}", fg="cyan"))
    click.echo()

def print_no_server_selected_error():
    click.echo(click.style("No server selected. Select a server with qpcli login [server] [server-name] or qpcli set server [server-name].", fg="red"))

# Commands
@click.group()
def list():
    pass


@list.command()
def servers():
    print_current_info()
    click.echo("List of servers:")
    for name in config.sections():
        if name == config["DEFAULT"]["server"]:
            click.echo(click.style(f"  * {name}", fg="green"))
        else:
            click.echo(f"    {name}")

    if len(config.sections()) == 0:
        click.echo(click.style("No servers added. Setup a server connection with qpcli login [server] [server-name].", fg="red"))
    if config.get("DEFAULT", "server") == "none":
        click.echo()
        print_no_server_selected_error()


@list.command()
def users():
    if config.get("DEFAULT", "server") == "none":
        print_no_server_selected_error()
        return

    qportalclient = util.get_qportalclient()

    print_current_info()
    click.echo("List of users:")
    for student in qportalclient.get_students():
        if student.id == config["DEFAULT"]["user_id"]:
            click.echo(click.style(f"  * ({student.id}) {student.name}", fg="green"))
        else:
            click.echo(f"    {student.name}")

@list.command()
def classes():
    if config.get("DEFAULT", "server") == "none":
        print_no_server_selected_error()
        return

    qportalclient = util.get_qportalclient()

    print_current_info()

    click.echo(click.style(f"Hint: Use qpcli list assignments [class id] to see assignments.", fg="green"))
    click.echo(click.style(f"e.g. for AP Eng Lang/Comp (HP) (694200):", fg="green"))
    click.echo(click.style(f"qpcli list assignments 694200", fg="green"))
    click.echo(click.style(f"You can also list all assignments with:", fg="green"))
    click.echo(click.style(f"qpcli list assignements", fg="green"))

    click.echo()
    click.echo("List of classes:")
    click.echo()

    for q_class in qportalclient.get_assignments():
        print_class(q_class)

@list.command()
@click.argument("class_id", default=None, required=False)
def assignments(class_id):
    if config.get("DEFAULT", "server") == "none":
        print_no_server_selected_error()
        return

    print_current_info()
    qportalclient = util.get_qportalclient()

    for q_class in qportalclient.get_assignments():
        if class_id is not None:
            if class_id != q_class.name.split('(')[-1][:-1]:
                continue

        print_class(q_class)

        for assignment in q_class.assignments:
            fmtstr = f"Assigned {assignment.assigned} Due {assignment.due} {assignment.name} {assignment.score}/{assignment.total_pts} ({round(assignment.pct, 3)}); {assignment.comments}"

            if assignment.pct >= 90:
                click.echo(click.style(fmtstr, fg="green"))
            elif assignment.pct >= 80:
                click.echo(click.style(fmtstr, fg="yellow"))
            elif assignment.pct >= 70:
                click.echo(click.style(fmtstr, fg="bright_yellow"))
            elif assignment.pct >= 60:
                click.echo(click.style(fmtstr, fg="red"))
            else:
                click.echo(click.style(fmtstr, fg="bright_red"))

        click.echo()
