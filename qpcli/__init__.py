from qportalwrapper.qportalwrapper import QPortalWrapper, QError
import click
from pathlib import Path
import configparser

from qpcli.list import list as qp_list
from qpcli.set import set as qp_set
from qpcli.config import config, CONFIG_FILE

try:
    config["DEFAULT"]
except KeyError:
    config["DEFAULT"] = {}

try:
    config["DEFAULT"]["server"]
except KeyError:
    config["DEFAULT"]["server"] = "none"

try:
    config["DEFAULT"]["user_id"]
    config["DEFAULT"]["user"]
except KeyError:
    config["DEFAULT"]["user_id"] = "-1"
    config["DEFAULT"]["user"] = "none"

with open(CONFIG_FILE, "w+") as f:
    config.write(f)

@click.group()
def qpcli():
    pass

@qpcli.command()
@click.option("--id", default=None)
@click.option("--password", default=None)
@click.option("--no-set-server", default=False)
@click.option("--no-set-user", default=False)
@click.argument("server")
@click.argument("server-name")
def login(id, password, no_set_server, no_set_user, server, server_name):
    """
    Setup credentials to Q Student.
    """
    user = id

    if user is None:
        user = input("Enter ID: ")
    if password is None:
        password = input("Enter password: ")

    config[server_name] = {}
    config[server_name]["server"] = server
    config[server_name]["user"] = user
    config[server_name]["password"] = password

    if not no_set_server:
        config["DEFAULT"]["server"] = server_name
    if not no_set_user:
        server = config[config["DEFAULT"]["server"]]
        qportalclient = QPortalWrapper(server["server"])
        qportalclient.login(server["user"],
                            server["password"])
        config["DEFAULT"]["user_id"] = qportalclient.students[0].id
        config["DEFAULT"]["user"] = qportalclient.students[0].name

    click.echo("Saving user and password unencrypted under $HOME/.config/qpcli.conf.")

    with open(CONFIG_FILE, "w+") as f:
        config.write(f)

qpcli.add_command(qp_list)
qpcli.add_command(qp_set)

if __name__ == "__main__":
    qpcli()
