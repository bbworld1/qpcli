import click
from qpcli.config import config
from qportalwrapper.qportalwrapper import QPortalWrapper

def print_no_server_selected_error():
    click.echo(click.style("No server selected. Select a server with qpcli login [server] [server-name] or qpcli set server [server-name].", fg="red"))

def get_qportalclient():
    server = config[config["DEFAULT"]["server"]]

    qportalclient = QPortalWrapper(server["server"])
    qportalclient.login(server["user"],
                        server["password"])
    qportalclient.select_student(config["DEFAULT"]["user_id"])

    return qportalclient
