import click
from qpcli.config import config, CONFIG_FILE
from qportalwrapper.qportalwrapper import QPortalWrapper
from qportalwrapper.datatypes import Assignment, Class
from qpcli import util
from util import print_no_server_selected_error

# Common functions
def print_current_info():
    server = config[config['DEFAULT']['server']]
    click.echo(click.style(f"Current server: {config['DEFAULT']['server']} ({server['server']})", fg="bright_cyan"))
    click.echo(click.style(f"Current user: {config['DEFAULT']['user']}", fg="bright_cyan"))
    click.echo()

# Commands
@click.group()
def show():
    pass

@show.command()
@click.option("--parsable", default=False)
@click.argument("id")
def name(parsable, id):
    if config.get("DEFAULT", "server") == "none":
        click.echo()
        print_no_server_selected_error()

    qportalclient = util.get_qportalclient()
    qportalclient.set_student(id)

    if not parsable:
        print_current_info()
        click.echo(f"Name for {id}: {qportalclient.current_student}")
    else:
        click.echo(f"{qportalclient.current_student}")
