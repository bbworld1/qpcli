from pathlib import Path
import configparser

CONFIG_FILE = str(Path.home()) + "/.config/qpcli.conf"
config = configparser.ConfigParser()
config.read(CONFIG_FILE)
