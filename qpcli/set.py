import click
from qpcli.config import config, CONFIG_FILE
from qportalwrapper.qportalwrapper import QPortalWrapper
from qportalwrapper.datatypes import Assignment, Class
from qpcli import util
from qpcli.util import print_no_server_selected_error

# Commands
@click.group()
def set():
    pass


@set.command()
@click.argument("server_name")
def server(server_name):
    config["DEFAULT"]["server"] = server_name
    server_addr = config[server_name]["server"]
    click.echo(click.style(f"Q Server set to {server_name} ({server_addr}).", fg="green"))

@set.command()
@click.argument("user_id")
def user(user_id):
    if config.get("DEFAULT", "server") == "none":
        click.echo()
        print_no_server_selected_error()

    qportalclient = util.get_qportalclient()
    qportalclient.select_student(user_id)

    config["DEFAULT"]["user_id"] = user_id
    config["DEFAULT"]["user"] = qportalclient.selected_student.name

    click.echo(click.style(f"Q User/student set to {user_id} ({config['DEFAULT']['user']}).", fg="green"))
