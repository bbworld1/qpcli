# qpcli

qpcli is the unofficial Q Student Portal command-line client.
You can get your grades very simply through this CLI:

```
qpcli login <your Q server> <name>
qpcli list assignments
```

You can also show classes, etc. More functionality, including a
parseable output mode for integration with app bars and more, is
planned in the very near future.

use `qpcli --help` for full details.

## Installation

It's just a Python app.

Install requirements via `pip3 install -r requirements.txt`
Install via `python3 setup.py install`
